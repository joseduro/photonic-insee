/**
* @file
* @brief	k-ary n-tree topology tools.
*
*@author Javier Navaridas

FSIN Functional Simulator of Interconnection Networks
Copyright (2003-2011) J. Miguel-Alonso, J. Navaridas

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include "globals.h"

/**
* Creates a fat tree topology.
*
* This function defines all the links between the elements in the network.
*/
void create_fattree(){
	long i, j, nr, np;	//neighbor router and port.
	long st, r, p;		//current stage, router and port.
	long st_first, next_st_first;	//if for this and next stage's first element(switch).
	long k, ks, ks1, sg;	//auxiliar variables for saving cpu and reducing complexity.
	long router_per_stage;	// Total Number of routers in each stage of a multistage topology.

	k=radix/2;
	router_per_stage=(long)pow(k, nstages-1);
//	route = (channel_link **) alloc (sizeof(channel_link *) * (router_per_stage * (nstages-1) + nprocs) );

	// Initializating processors. Only 1 transit port plus injection queues.
	for (i=0; i<nprocs; i++){
		network[i].rcoord[STAGE]=-1;
		network[i].rcoord[POSITION]=i;
		for (j=0; j<ninj; j++)
			inj_init_queue(&network[i].qi[j]);
		init_ports(i);
		nr=(i/k)+nprocs;
		np=(i%k)+k;
		network[i].nbor[0] = nr;
		network[i].nborp[0] = np;
		network[i].op_i[0] = ESCAPE;
		network[nr].nbor[np] = i;
		network[nr].nborp[np] = 0;
		network[nr].op_i[np] = ESCAPE;
		for (j=1; j<radix; j++){
			network[i].nbor[j] = NULL_PORT;
			network[i].nborp[j] = NULL_PORT;
			network[i].op_i[j] = ESCAPE;
		}
	}
	next_st_first=nprocs;
	ks=1;	// k ^ stage
	ks1=k;	// k ^ stage+1

	// Initializing switches. No injection queues needed.
	for (st=0; st< nstages-1; st++ ){
		st_first = next_st_first;						//first swith in this stage
		next_st_first = st_first + router_per_stage;	//first switch in the next stage
		for (r=0; r < router_per_stage; r++ ){
			i = r + st_first;
			network[i].rcoord[STAGE]=st;
			network[i].rcoord[POSITION]=r;
			init_ports(i);
			sg = r/(ks1);
			for (p=0; p< k; p++) {
				nr = mod(((p*ks) + (ks1*sg)+(r%ks)), router_per_stage) + next_st_first;
				np = mod(((r/ks)-(ks1*sg)), k) + k;

				network[i].nbor[p]=nr;			// neighbor router
				network[i].nborp[p]=np;			// neighbor's port
				network[i].op_i[p] = ESCAPE;
				network[nr].nbor[np]=i;			// neighbor router's neighbor
				network[nr].nborp[np]=p;		// neighbor router's neighbor's port
				network[nr].op_i[np] = ESCAPE;
			}
		}
		ks=ks1;
		ks1=ks1*k;
	}
	st_first=next_st_first;

	// last stage routers' neighbor is the same router
	for (r=0; r< router_per_stage; r++ ){
		i = r + st_first;
		network[i].rcoord[STAGE]=st;
		network[i].rcoord[POSITION]=r;
		init_ports(i);
		for (p=0; p< k; p++) {
			network[i].nbor[p] = NULL_PORT;	// neighbor router
			network[i].nborp[p] = NULL_PORT;// neighbor's port
			network[i].op_i[p] = NULL_PORT;
		}
	}
}

/**
* Creates a thin tree topology.
*
* This function defines all the links between the elements in the network.
*/
void create_thintree()
{
	long i, j, nr, np;	//neighbor router and port.
	long st, r, p;	//current stage, router and port.
	long st_first, next_st_first;	//if for this and next stage's first element(switch).
	long sgUp;		// stUp ^ st
	long router_per_stage;		// Total Number of routers in each stage of a multistage topology.

	// Processors
	for (i=0; i<nprocs; i++){
		network[i].rcoord[STAGE]=-1;
		network[i].rcoord[POSITION]=i;
		for (j=0; j<ninj; j++) inj_init_queue(&network[i].qi[j]);
		init_ports(i);
		nr=(i/stDown)+nprocs;
		np=(i%stDown)+stUp;
		network[i].nbor[0] = nr;
		network[i].nborp[0] = np;
		network[i].op_i[0] = ESCAPE;
		network[nr].nbor[np] = i;
		network[nr].nborp[np] = 0;
		network[nr].op_i[np] = ESCAPE;
		for (j=1; j<radix; j++){
			network[i].nbor[j] = NULL_PORT;
			network[i].nborp[j] = NULL_PORT;
			network[i].op_i[j] = ESCAPE;
		}
	}

	next_st_first=nprocs;
	sgUp=1;		//stUp ^ st
	router_per_stage=(long)pow(stDown,nstages-1);
		// Initializing switches. No injection queues needed.
	for (st=0; st< nstages-1; st++ ){
		st_first = next_st_first;						//first swith in this stage
		next_st_first = st_first + router_per_stage;	//first switch in the next stage
		for (r=0; r < router_per_stage; r++ ){
			i = r + st_first;
			network[i].rcoord[STAGE]=st;
			network[i].rcoord[POSITION]=r;
			init_ports(i);
			for (p=0; p< stUp; p++) {
				nr = (p*sgUp) + (r%sgUp) + ((r/(stDown*sgUp))*sgUp*stUp) + next_st_first;
				np = ((r/sgUp)%stDown)+ stUp;
				network[i].nbor[p]=nr;		// neighbor router
				network[i].nborp[p]=np;		// neighbor's port
				network[i].op_i[p] = ESCAPE;
				network[nr].nbor[np]=i;		// neighbor router's neighbor
				network[nr].nborp[np]=p;	// neighbor router's neighbor's port
				network[nr].op_i[np] = ESCAPE;
			}
		}
		sgUp = sgUp * stUp;
		router_per_stage = (stUp * router_per_stage)/stDown;
	}
	st_first=next_st_first;
	// last stage routers' neighbor is the same router
	for (r=0; r< router_per_stage; r++ ){
		i = r + st_first;
		network[i].rcoord[STAGE]=st;
		network[i].rcoord[POSITION]=r;
		init_ports(i);
		for (p=0; p< stUp; p++) {
			network[i].nbor[p] = NULL_PORT;	// neighbor router
			network[i].nborp[p] = NULL_PORT;// neighbor's port
			network[i].op_i[p] = NULL_PORT;
		}
	}
}

/**
* Creates a slim tree topology.
*
* This function defines all the links between the elements in the network.
*/
void create_slimtree(){
	long i, j, nr, np;	//neighbor router and port.
	long st, r, p;					//current stage, router and port.
	long st_first, next_st_first;	//if for this and next stage's first element(switch).
	long router_per_stage,
	     router_per_next_stage;		// Total Number of routers in each stage of a multistage topology.

	for (i=0; i<nprocs; i++){
		for (j=0; j<ninj; j++) inj_init_queue(&network[i].qi[j]);
		network[i].rcoord[STAGE]=-1;
		network[i].rcoord[POSITION]=i;
		init_ports(i);
		nr=(i/stDown)+nprocs;
		np=(i%stDown)+stUp;
		network[i].nbor[0] = nr;
		network[i].nborp[0] = np;
		network[i].op_i[0] = ESCAPE;
		network[nr].nbor[np] = i;
		network[nr].nborp[np] = 0;
		network[nr].op_i[np] = ESCAPE;
		for (j=1; j<radix; j++){
			network[i].nbor[j] = NULL_PORT;
			network[i].nborp[j] = NULL_PORT;
			network[i].op_i[j] = ESCAPE;
		}
	}

	router_per_stage=(long)pow((stDown/stUp),nstages-1)*stUp;
	router_per_next_stage=(router_per_stage*stUp)/stDown;
	st_first=nprocs;
	next_st_first=st_first+router_per_stage;

	for (st=0; st<nstages-1; st++){
		for (r=0; r<router_per_stage;r++){
			i = r + st_first;
			network[i].rcoord[STAGE]=st;
			network[i].rcoord[POSITION]=r;
			init_ports(i);

			for (p=0; p<stUp; p++){
				nr=stUp*(r/stDown)+p+next_st_first;
				np=r%stDown+stUp;
				network[i].nbor[p]=nr;
				network[i].nborp[p]=np;
				network[nr].nbor[np]=i;
				network[nr].nborp[np]=p;
			}
		}
		router_per_stage=router_per_next_stage;
		router_per_next_stage=(router_per_next_stage*stUp)/stDown;
		st_first=next_st_first;
		next_st_first=st_first + router_per_stage;
    }

	// last stage routers' neighbor is the same router
	for (r=0; r< router_per_stage; r++ ){
		i = r + st_first;
		network[i].rcoord[STAGE]=st;
		network[i].rcoord[POSITION]=r;
		init_ports(i);
		for (p=0; p< stUp; p++) {
			network[i].nbor[p] = NULL_PORT;	// neighbor router
			network[i].nborp[p] = NULL_PORT;// neighbor's port
			network[i].op_i[p] = NULL_PORT;
		}
	}
}

/**
* Generates the routing record for a k-ary n-tree.
*
* This function allows adaptive routing because no route is defined here.
*
* @param source The source node of the packet.
* @param destination The destination node of the packet.
* @return The routing record needed to go from source to destination.
*/
routing_r fattree_rr_adapt (long source, long destination) {
	long nhops=1,	// Number of hops
		k=radix/2,	// Number of ports
		k_n=k;		// k ^ nhops
	routing_r res;
	if (source == destination)
		panic("Self-sent packet");

	// Search the first common ancester
	while (source / k_n != destination / k_n){
		nhops++;
		k_n=k_n*k;
	}
	res.rr=NULL;
	res.size=nhops*2;
	return res;
}

/**
* Generates the routing record for a thin tree.
*
* This function allows adaptive routing because no route is defined here.
*
* @param source The source node of the packet.
* @param destination The destination node of the packet.
* @return The routing record needed to go from source to destination.
*/
routing_r thintree_rr_adapt (long source, long destination) {
	long nhops=1, sgDown=stDown;
	routing_r res;

	if (source == destination)
		panic("Self-sent packet");

	// Search the first common ancester
	while (source / (sgDown) != destination / (sgDown)){
		nhops++;
		sgDown=sgDown*stDown;
	}

	res.rr=NULL;
	res.size=nhops*2;
	return res;
}

/**
* Generates the routing record for a slimtree.
*
* This function allows adaptive routing because no route is defined here.
*
* @param source The source node of the packet.
* @param destination The destination node of the packet.
* @return The routing record needed to go from source to destination.
*/
routing_r slimtree_rr_adapt (long source, long destination) {
	long nhops=1,	// Number of hops
		sgDown,		// stDown ^ nhops
		sgUp;		// StUp ^ nhops -2
	routing_r res;

	if (source == destination)
		panic("Self-sent packet");

	// Search the first common ancester
	if (source/ stDown != destination /stDown){
		nhops=2;
		sgDown=stDown*stDown;
		sgUp=1;
		while ((sgUp*source)/sgDown != ((sgUp*destination)/sgDown)){
			nhops++;
			sgDown=sgDown*stDown;
			sgUp=sgUp*stUp;
		}
	}
	res.rr=NULL;
	res.size=nhops*2;
	return res;
}

bool_t search_route_tree(long source, long destination, long pkt_id){

}

bool_t search_route_tree_adaptative(long source, long destination, long pkt_id){

	long distance = pkt_space[pkt_id].rr.size;
	long hops = pkt_space[pkt_id].n_hops;
	port_type port;
	long node = source;
	node_route * aux_route;
	aux_route = alloc(sizeof(node_route)*(distance+1));
	long channel;

	while (distance != hops){
		// in a fattree: stDown == k == stUP;
		if(plevel & 4) printf("Hop %ld, Node %ld -> ",hops,node);
		if (hops==0){	// NIC
			port=min_channel_occupation(node,0, nchan);
			channel = get_first_channel(node, port);
			if(channel == -1){
				free(aux_route);
				return B_FALSE;
			}
			set_node_route(aux_route, hops, node, port, network[node].nborp[port] , channel);
			hops++;
			node=network[node].nbor[port];

		}else if (hops < (distance/2)){ 								//going Up, (adaptive)
			port=min_channel_occupation(node,0, stDown); // We will search in all the Upward links.
			channel=get_first_channel(node,port);
			if(channel == -1){
				free(aux_route);
				return B_FALSE;
			}
			set_node_route(aux_route, hops, node, port, network[node].nborp[port] , channel);
			hops++;
			node=network[node].nbor[port];
		}else{ 	// going down static.
			port=min_channel_occupation(node,
				(((destination / (long)pow(stDown, network[node].rcoord[STAGE])) % stDown )+ stDown),
				(((destination / (long)pow(stDown, network[node].rcoord[STAGE])) % stDown )+ stDown + 1));
			channel=get_first_channel(node,port);
			if(channel == -1){
				free(aux_route);
				return B_FALSE;
			}
			set_node_route(aux_route, hops, node, port, network[node].nborp[port] , channel);
			hops++;
			node=network[node].nbor[port];
		}
		if(plevel & 4) printf("Node %ld, Port %ld\tDistance-hops %ld-%ld\n",node,port,distance,hops);

	}
	for(channel = 0; channel < n_channels; channel++){
		if(plevel & 4) printf("Node %ld, Channel %ld\tDistance-hops %ld-%ld\n",node,port,distance,hops);
		if(hasFreeLinkChannelN(node,p_con,channel)){
			if(plevel & 4) printf(" CONSUME Channel:%ld\n",channel);
			set_node_route(aux_route,hops,node,p_con,-1,channel);
			set_route(aux_route,distance,pkt_id, channel);
			free(aux_route);
			return B_TRUE;
		}
	}
	return B_FALSE;

}

long get_first_channel(long node, port_type port){
	long c;
	for(c=0; c < n_channels; c++){
		if(route[node][port].lamda[c] == -1)
			return c;
	}
	return -1;

}

port_type min_channel_occupation(long node,port_type first, port_type last){
	long max=0;		                				// Max value
	long ql=0;
	long c;											// Queue length
	port_type p, nbp[last-first], nm=0;				// Neighbor port

	for (p=first; p<last; p++){
		for (c=0; c < n_channels; c++){
			if (route[node][p].lamda[c] == -1){
				ql++;
			}
		}

		if (ql>max){
			max=ql;
			nm=1;
			nbp[0]=p;
		}
		if (ql==max)
			nbp[nm++]=p;
		ql=0;
	}
	return nbp[rand()%nm];
}
