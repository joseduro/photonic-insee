/**
* @file
* @brief	The torus & mesh topologies tools.

FSIN Functional Simulator of Interconnection Networks
Copyright (2003-2011) J. Miguel-Alonso, A. Gonzalez, J. Navaridas

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include "globals.h"

/**
* Obtains a neighbor node in torus & mesh topologies.
*
* Given a node address "ad", a direction "wd" (X,Y or Z) and a way "ww" (UP or DOWN)
* returns the address of the neighbor in that direction and way; only valid for torus
* but usable also for mesh.
* 
* @param ad A node address.
* @param wd A dimension (X,Y or Z).
* @param ww A way (UP or DOWN).
* @return The address of the neighbor in that direction.
*/
long torus_neighbor (long ad, dim wd, way ww) {
	long ox, oy, oz;
	long nx, ny, nz;
	long w;

	nx = ny = nz = 0;
	if (ww == UP)
		w = 1;
	else
		w = -1;

	ox=network[ad].rcoord[D_X];
	oy=network[ad].rcoord[D_Y];
	oz=network[ad].rcoord[D_Z];

	switch (wd) {
		case D_X:
			nx = (ox+w)%nodes_x;
			if (nx < 0)
				nx += nodes_x;
			ny = oy;
			nz = oz;
			break;
		case D_Y:
			ny = (oy+w)%nodes_y;
			if (ny < 0)
				ny += nodes_y;
			nx = ox;
			nz = oz;
			break;
		case D_Z:
			nz = (oz+w)%nodes_z;
			if (nz < 0)
				nz += nodes_z;
			nx = ox;
			ny = oy;
			break;
		case INJ:
		case CON:
		default:;
	}
	return address(nx, ny, nz);
}

/**
* Generates the routing record for a mesh.
* 
* @param source The source node of the packet.
* @param destination The destination node of the packet.
* @return The routing record needed to go from source to destination.
*/
routing_r mesh_rr (long source, long destination) {
	long sx, sy, sz, dx, dy, dz;
	routing_r res;

	res.rr=alloc(ndim*sizeof(long));

	if (source == destination)
		panic("Self-sent packet");

	sx=network[source].rcoord[D_X];
	sy=network[source].rcoord[D_Y];
	sz=network[source].rcoord[D_Z];

	dx=network[destination].rcoord[D_X];
	dy=network[destination].rcoord[D_Y];
	dz=network[destination].rcoord[D_Z];

	res.rr[D_X] = dx-sx;
	res.size = abs(res.rr[D_Z]);

	if (ndim >= 2){
		res.rr[D_Y] = dy-sy;
		res.size += abs(res.rr[D_Y]);
	}

	if (ndim == 3){
		res.rr[D_Z] = dz-sz;
		res.size += abs(res.rr[D_Z]);
	}

	return res;
}

/**
* Generates the routing record for a torus.
* 
* @param source The source node of the packet.
* @param destination The destination node of the packet.
* @return The routing record needed to go from source to destination.
*/
routing_r torus_rr (long source, long destination) {
	long sx, sy, sz, dx, dy, dz;
	routing_r res;

	res.rr=alloc(ndim*sizeof(long));

	if (source == destination)
		panic("Self-sent packet");

	sx=network[source].rcoord[D_X];
	sy=network[source].rcoord[D_Y];
	sz=network[source].rcoord[D_Z];

	dx=network[destination].rcoord[D_X];
	dy=network[destination].rcoord[D_Y];
	dz=network[destination].rcoord[D_Z];

	res.rr[D_X] = (dx-sx)%nodes_x;
	if (res.rr[D_X] < 0)
		res.rr[D_X] += nodes_x;
	if (res.rr[D_X] > nodes_x/2)
		res.rr[D_X] = (nodes_x-res.rr[D_X])*(-1);
	if ((double)res.rr[D_X] == nodes_x/2.0)
		if (rand() >= (RAND_MAX/2))
			res.rr[D_X] = (nodes_x-res.rr[D_X])*(-1);
	res.size = abs(res.rr[D_X]);

	if (ndim >= 2) {
		res.rr[D_Y] = (dy-sy)%nodes_y;
		if (res.rr[D_Y] < 0)
			res.rr[D_Y] += nodes_y;
		if (res.rr[D_Y] > nodes_y/2)
			res.rr[D_Y] = (nodes_y-res.rr[D_Y])*(-1);
		if ((double)res.rr[D_Y] == nodes_y/2.0)
			if (rand() >= (RAND_MAX/2))
				res.rr[D_Y] = (nodes_y-res.rr[D_Y])*(-1);
		res.size += abs(res.rr[D_Y]);
	}

	if (ndim == 3) {
		res.rr[D_Z] = (dz-sz)%nodes_z;
		if (res.rr[D_Z] < 0)
			res.rr[D_Z] += nodes_z;
		if (res.rr[D_Z] > nodes_z/2)
			res.rr[D_Z] = (nodes_z-res.rr[D_Z])*(-1);
		if ((double)res.rr[D_Z] == nodes_z/2.0)
			if (rand() >= (RAND_MAX/2))
				res.rr[D_Z] = (nodes_z-res.rr[D_Z])*(-1);
		res.size += abs(res.rr[D_Z]);
	}
	return res;
}

/**
* Generates the routing record for an unidirectional torus.
* 
* @param source The source node of the packet.
* @param destination The destination node of the packet.
* @return The routing record needed to go from source to destination.
*/
routing_r torus_rr_unidir (long source, long destination) {
	long sx, sy, sz, dx, dy, dz;
	routing_r res;

	res.rr=alloc(ndim*sizeof(long));

	if (source == destination)
		panic("Self-sent packet");

	sx=network[source].rcoord[D_X];
	sy=network[source].rcoord[D_Y];
	sz=network[source].rcoord[D_Z];

	dx=network[destination].rcoord[D_X];
	dy=network[destination].rcoord[D_Y];
	dz=network[destination].rcoord[D_Z];

	res.rr[D_X] = (dx-sx)%nodes_x;
	if (res.rr[D_X] < 0)
		res.rr[D_X] += nodes_x;
	res.size = abs(res.rr[D_X]);

	if (ndim >= 2) {
		res.rr[D_Y] = (dy-sy)%nodes_y;
		if (res.rr[D_Y] < 0)
			res.rr[D_Y] += nodes_y;
		res.size += abs(res.rr[D_Y]);
	}

	if (ndim == 3){
		res.rr[D_Z] = (dz-sz)%nodes_z;
		if (res.rr[D_Z] < 0)
			res.rr[D_Z] += nodes_z;
		res.size += abs(res.rr[D_Z]);
	}
	return res;
}

#if (PHOTONIC != 0)

/*bool_t set_route_torus(long source, long destination, long channel, long pkt_id, long * distance ){
	long aux;
	long sx, sy, sz, dx, dy, dz;
	
	sx=network[source].rcoord[D_X];
	sy=network[source].rcoord[D_Y];
	sz=network[source].rcoord[D_Z];

	dx=network[destination].rcoord[D_X];
	dy=network[destination].rcoord[D_Y];
	dz=network[destination].rcoord[D_Z];
	
	
	if(source == destination){
		if(hasFreeLinkChannel(sx,sy,sz,p_con,channel)){
			route[destination][p_con].lamda[channel]=pkt_id;
			return B_TRUE;
		}else{
			return B_FALSE;
		}
	}


	//Puertos Route [+X,-X,+Y,-Y,+Z,-Z]
	if(sx!=dx){
		if( ((dx>sx) && ((dx-sx)<(nodes_x/2))) || 
			((dx<sx) && ((sx-dx)>(nodes_x/2))) ){ //Paquete en sentido +X
			aux = sx+1;
			if(aux == nodes_x)
				aux = 0;
			if(hasFreeLinkChannel(sx,sy,sz,0,channel) && hasFreeLinkChannel(aux,sy,sz,1,channel)){
				*distance+=1;
				if(set_route_torus(address(aux,sy,sz),destination, channel, pkt_id, distance)){
					if(plevel & 4) printf(" +X ");
					route[address(sx,sy,sz)][0].lamda[channel]=pkt_id;
					route[address(aux,sy,sz)][1].lamda[channel]=pkt_id;
					return B_TRUE;
				}else{
					*distance-=1;
				//	return B_FALSE;
				}
			}
		}else{ //Paquete en sentido -X
			aux = sx-1;
			if(aux < 0)
				aux = nodes_x-1;
			if(hasFreeLinkChannel(sx,sy,sz,1,channel) && hasFreeLinkChannel(aux,sy,sz,0,channel)){
				*distance+=1;
				if(set_route_torus(address(aux,sy,sz),destination, channel, pkt_id, distance)){
					if(plevel & 4) printf(" -X ");
					route[address(sx,sy,sz)][1].lamda[channel]=pkt_id;
					route[address(aux,sy,sz)][0].lamda[channel]=pkt_id;
					return B_TRUE;
				}else{
					*distance-=1;
				//	return B_FALSE;
				}
			}
		}
		
	}
	if(ndim >= 2 && sy != dy){
		if( ((dy>sy) && ((dy-sy)<(nodes_y/2))) || 
			((dy<sy) && ((sy-dy)>(nodes_y/2))) ){  //Paquete en sentido +Y
			aux = sy+1;
			if(aux == nodes_y)
				aux = 0;
			if(hasFreeLinkChannel(sx,sy,sz,2,channel) && hasFreeLinkChannel(sx,aux,sz,3,channel)){
				*distance+=1;
				if(set_route_torus(address(sx,aux,sz),destination, channel, pkt_id, distance)){
					if(plevel & 4) printf(" +Y ");
					route[address(sx,sy,sz)][2].lamda[channel]=pkt_id;
					route[address(sx,aux,sz)][3].lamda[channel]=pkt_id;
					return B_TRUE;
				}
				else{
					*distance-=1;
				//	return B_FALSE;
				}

			}
		}else{	//Paquete en sentido -Y
			aux = sy-1;
			if (aux < 0)
				aux = nodes_y-1;
			if(hasFreeLinkChannel(sx,sy,sz,3,channel) && hasFreeLinkChannel(sx,aux,sz,2,channel)){
				*distance+=1;
				if(set_route_torus(address(sx,aux,sz),destination, channel, pkt_id, distance)){
					if(plevel & 4) printf(" -Y ");
					route[address(sx,sy,sz)][3].lamda[channel]=pkt_id;
					route[address(sx,aux,sz)][2].lamda[channel]=pkt_id;
					return B_TRUE;
				}else{
					*distance-=1;
				//	return B_FALSE;
				}
			}
		}
	}
	if(ndim == 3 &&  sz != dz){
		if( ((dz>sz) && ((dz-sz)<(nodes_z/2))) || 
			((dz<sz) && ((sz-dz)>(nodes_z/2))) ){//Paquete en sentido +Z
			aux = sz+1;
			if(aux==nodes_z){

				aux=0;
			if(hasFreeLinkChannel(sx,sy,sz,4,channel) && hasFreeLinkChannel(sx,sy,aux,5,channel)){
				*distance+=1;
				if(set_route_torus(address(sx,sy,aux),destination, channel, pkt_id, distance)){
					if(plevel & 4) printf(" +Z ");
					route[address(sx,sy,sz)][4].lamda[channel]=pkt_id;
					route[address(sx,sy,aux)][5].lamda[channel]=pkt_id;
					return B_TRUE;
				}else{
					*distance-=1;
				//	return B_FALSE;
				}
			}
		}else{ //Paquete en sentido -Z
			aux = sz-1;
			if(aux<0)
				aux=nodes_z-1;
			if(hasFreeLinkChannel(sx,sy,sz,5,channel) && hasFreeLinkChannel(sx,sy,aux,4,channel)){
				*distance+=1;
				if(set_route_torus(address(sx,sy,aux),destination, channel, pkt_id, distance)){
					if(plevel & 4) printf(" -Z ");
					route[address(sx,sy,sz)][5].lamda[channel]=pkt_id;
					route[address(sx,sy,aux)][4].lamda[channel]=pkt_id;
					
					return B_TRUE;
				}else{
					*distance-=1;
				//	return B_FALSE;
				}
			}
		}
	}
	return B_FALSE;
	
}*/





/**
* 'Virtual' Function that performs the photonic route.
*
* @param source The source node.
* @param destination The destination node.
* @param pkt_id The packet id.
*
 *
 *@see search_route_torus
 *@see search_route_torus_adaptative
 *@see search_route_tree
 *@see search_route_tree_adaptative
 *@see search_route_dragonfy
 *
*/ 


bool_t search_route_torus(long source, long destination, long pkt_id){

	long aux,aux_done;
	long sx, sy, sz, dx, dy, dz;
	long channel;
	long distance = pkt_space[pkt_id].rr.size;
	long route_cont,i;

	dx=network[destination].rcoord[D_X];
	dy=network[destination].rcoord[D_Y];
	dz=network[destination].rcoord[D_Z];

	for(channel = 0; channel < n_channels; channel++){
		route_cont = 0;
		node_route * aux_route;
		routing_r aux_rr;
		aux_rr.rr = alloc( (ndim+1) * sizeof(long));
		for(i=0;i<ndim;i++){
			aux_rr.rr[i] = pkt_space[pkt_id].rr.rr[i];
		}
		//aux_rr.rr[D_Y] = pkt_space[pkt_id].rr.rr[D_Y];
		//aux_rr.rr[D_Z] = pkt_space[pkt_id].rr.rr[D_Z]; 
		aux_route = alloc(sizeof(node_route)*(distance+1));
		bool_t exit = B_FALSE;

		sx=network[source].rcoord[D_X];
		sy=network[source].rcoord[D_Y];
		sz=network[source].rcoord[D_Z];
		
		while(!exit){
			aux_done=0;
			//Puertos Route [+X,-X,+Y,-Y,+Z,-Z]
			if(aux_rr.rr[D_X] != 0){
				if( aux_rr.rr[D_X] > 0){ //Paquete en sentido +X
					aux = sx+1;
					if(aux == nodes_x)
						aux = 0;
					if(hasFreeLinkChannel(sx,sy,sz,dir(D_X,UP),channel)){
						set_node_route(aux_route,route_cont,address(sx,sy,sz),dir(D_X,UP),dir(D_X,DOWN),channel);
						route_cont++;
						sx=aux;
						aux_rr.rr[D_X]--;
						aux_done++;
						if(plevel & 4) printf(" +X ");
					}
				}else{ //Paquete en sentido -X
					aux = sx-1;
					if(aux < 0)
						aux = nodes_x-1;
					if(hasFreeLinkChannel(sx,sy,sz,dir(D_X,DOWN),channel)){
						set_node_route(aux_route,route_cont,address(sx,sy,sz),dir(D_X,DOWN),dir(D_X,UP),channel);
						route_cont++;
						sx=aux;
						aux_rr.rr[D_X]++;
						aux_done++;
						if(plevel & 4) printf(" -X ");
						}
				}
			}
			if(ndim >= 2 && aux_rr.rr[D_Y] != 0 && aux_done == 0){
				if( aux_rr.rr[D_Y] > 0 ){  //Paquete en sentido +Y
					aux = sy+1;
					if(aux == nodes_y)
						aux = 0;
					if(hasFreeLinkChannel(sx,sy,sz,dir(D_Y,UP),channel)){
						set_node_route(aux_route,route_cont,address(sx,sy,sz),dir(D_Y,UP),dir(D_Y,DOWN),channel);
						route_cont++;
						sy=aux;
						aux_rr.rr[D_Y]--;
						aux_done++;
						if(plevel & 4) printf(" +Y ");

					}
				}else{	//Paquete en sentido -Y
					aux = sy-1;
					if (aux < 0)
						aux = nodes_y-1;
					if(hasFreeLinkChannel(sx,sy,sz,dir(D_Y,DOWN),channel)){
						set_node_route(aux_route,route_cont,address(sx,sy,sz),dir(D_Y,DOWN),dir(D_Y,UP),channel);
						route_cont++;
						sy=aux;
						aux_rr.rr[D_Y]++;
						aux_done++;
						if(plevel & 4) printf(" -Y ");
					}
				}
			}
			if(ndim == 3 &&  aux_rr.rr[D_Z] != 0 && aux_done == 0){
				if( aux_rr.rr[D_Z] > 0 ){//Paquete en sentido +Z
					aux = sz+1;
					if(aux==nodes_z)
						aux=0;
					if(hasFreeLinkChannel(sx,sy,sz,dir(D_Z,UP),channel)){
						set_node_route(aux_route,route_cont,address(sx,sy,sz),dir(D_Z,UP),dir(D_Z,DOWN),channel);
						route_cont++;
						sz=aux;
						aux_rr.rr[D_Z]--;
						aux_done++;
						if(plevel & 4) printf(" +Z ");

					}
				}else{ //Paquete en sentido -Z
					aux = sz-1;
					if(aux<0)
						aux=nodes_z-1;
					if(hasFreeLinkChannel(sx,sy,sz,dir(D_Z,DOWN),channel)){
						set_node_route(aux_route,route_cont,address(sx,sy,sz),dir(D_Z,DOWN),dir(D_Z,UP),channel);
						route_cont++;
						sz=aux;
						aux_rr.rr[D_Z]++;
						aux_done++;
						if(plevel & 4) printf(" -Z ");
					}
				}
			}

			if(address(sx,sy,sz) == address(dx,dy,dz)){
				if(hasFreeLinkChannel(sx,sy,sz,p_con,channel)){
					set_node_route(aux_route,route_cont,address(sx,sy,sz),p_con,-1,channel);
					set_route(aux_route,distance,pkt_id, channel);
					free(aux_route);
					free(aux_rr.rr);
					return B_TRUE;
				}
			}
			if(aux_done==0) exit = B_TRUE ;
		}
		free(aux_route);
		free(aux_rr.rr);
	}
	return B_FALSE;

}



/**
* 'Virtual' Function that performs the photonic route.
*
* @param source The source node.
* @param destination The destination node.
* @param pkt_id The packet id.
*
 *
 *@see search_route_torus
 *@see search_route_torus_adaptative
 *@see search_route_tree
 *@see search_route_tree_adaptative
 *@see search_route_dragonfy
 *
*/ 


bool_t search_route_torus_adaptative(long source, long destination, long pkt_id){

	long aux,aux_done;
	long sx, sy, sz, dx, dy, dz;
	long channel = 0;
	long route_cont,i;
	long distance;

	dx=network[destination].rcoord[D_X];
	dy=network[destination].rcoord[D_Y];
	dz=network[destination].rcoord[D_Z];

	route_cont = 0;
	node_route * aux_route;
	routing_r aux_rr = torus_rr(source, destination);
	distance = aux_rr.size;
	aux_route = alloc(sizeof(node_route)*(distance+1));
	bool_t exit = B_FALSE;

	sx=network[source].rcoord[D_X];
	sy=network[source].rcoord[D_Y];
	sz=network[source].rcoord[D_Z];
		
	while(!exit){
		aux_done=0;
	if(plevel & 4)	printf("PKT->%ld, Nodo-> %ld, Destino-> %ld\tVuelta %ld, sx -> %ld, sy -> %ld, sz -> %ld\tChannel->%ld\tdx -> %ld, dy -> %ld, dz -> %ld\n",pkt_id,source,destination,route_cont,sx,sy,sz,channel, dx, dy, dz);
		//Puertos Route [+X,-X,+Y,-Y,+Z,-Z]
		if(aux_rr.rr[D_X] != 0){
			for(channel = 0; channel < n_channels && aux_done == 0; channel++){
				if( aux_rr.rr[D_X] > 0){ //Paquete en sentido +X
//						printf("ENTRA 1\n");
					aux = sx+1;
					if(aux == nodes_x)
						aux = 0;
					if(hasFreeLinkChannel(sx,sy,sz,dir(D_X,UP),channel)){
						set_node_route(aux_route,route_cont,address(sx,sy,sz),dir(D_X,UP),dir(D_X,DOWN),channel);
						route_cont++;
						sx=aux;
						aux_rr.rr[D_X]--;
						aux_done++;
						if(plevel & 4) printf(" +X Channel:%ld\n",channel);
					}
				}else{ //Paquete en sentido -X
//						printf("ENTRA 2\n");
					aux = sx-1;
					if(aux < 0)
						aux = nodes_x-1;
					if(hasFreeLinkChannel(sx,sy,sz,dir(D_X,DOWN),channel)){
						set_node_route(aux_route,route_cont,address(sx,sy,sz),dir(D_X,DOWN),dir(D_X,UP),channel);
						route_cont++;
						sx=aux;
						aux_rr.rr[D_X]++;
						aux_done++;
						if(plevel & 4) printf(" -X Channel:%ld\n",channel);
						}
				}
			}
		}
		if(ndim >= 2 && aux_rr.rr[D_Y] != 0 && aux_done == 0){
			for(channel = 0; channel < n_channels && aux_done == 0; channel++){
				if( aux_rr.rr[D_Y] > 0 ){  //Paquete en sentido +Y
//						printf("ENTRA 3");
					aux = sy+1;
					if(aux == nodes_y)
						aux = 0;
					if(hasFreeLinkChannel(sx,sy,sz,dir(D_Y,UP),channel)){
						set_node_route(aux_route,route_cont,address(sx,sy,sz),dir(D_Y,UP),dir(D_Y,DOWN),channel);
						route_cont++;
						sy=aux;
						aux_rr.rr[D_Y]--;
						aux_done++;
						if(plevel & 4) printf(" +Y Channel:%ld\n",channel);

					}
				}else{	//Paquete en sentido -Y
//						printf("ENTRA 4");
					aux = sy-1;
					if (aux < 0)
						aux = nodes_y-1;
					if(hasFreeLinkChannel(sx,sy,sz,dir(D_Y,DOWN),channel)){
						set_node_route(aux_route,route_cont,address(sx,sy,sz),dir(D_Y,DOWN),dir(D_Y,UP),channel);
						route_cont++;
						sy=aux;
						aux_rr.rr[D_Y]++;
						aux_done++;
						if(plevel & 4) printf(" -Y Channel:%ld\n",channel);
					}
				}
			}
		}
		if(ndim == 3 &&  aux_rr.rr[D_Z] != 0 && aux_done == 0){
			for(channel = 0; channel < n_channels && aux_done == 0; channel++){
				if( aux_rr.rr[D_Z] > 0 ){//Paquete en sentido +Z
//						printf("ENTRA 5");
					aux = sz+1;
					if(aux==nodes_z)
						aux=0;
					if(hasFreeLinkChannel(sx,sy,sz,dir(D_Z,UP),channel)){
						set_node_route(aux_route,route_cont,address(sx,sy,sz),dir(D_Z,UP),dir(D_Z,DOWN),channel);
						route_cont++;
						sz=aux;
						aux_rr.rr[D_Z]--;
						aux_done++;
						if(plevel & 4) printf(" +Z Channel:%ld\n",channel);

					}
				}else{ //Paquete en sentido -Z
//						printf("ENTRA 6");
					aux = sz-1;
					if(aux<0)
						aux=nodes_z-1;
					if(hasFreeLinkChannel(sx,sy,sz,dir(D_Z,DOWN),channel)){
						set_node_route(aux_route,route_cont,address(sx,sy,sz),dir(D_Z,DOWN),dir(D_Z,UP),channel);
						route_cont++;
						sz=aux;
						aux_rr.rr[D_Z]++;
						aux_done++;
						if(plevel & 4) printf(" -Z Channel:%ld\n",channel);
					}
				}
			}
		}

		if(address(sx,sy,sz) == address(dx,dy,dz)){
			for(channel = 0; channel < n_channels && aux_done == 0; channel++){
				if(hasFreeLinkChannel(sx,sy,sz,p_con,channel)){
					if(plevel & 4) printf(" CONSUME Channel:%ld\n",channel);
					set_node_route(aux_route,route_cont,address(sx,sy,sz),p_con,-1,channel);
					set_route(aux_route,distance,pkt_id, channel);
					free(aux_route);
					free(aux_rr.rr);
					return B_TRUE;
				}
			}
		}
		if(aux_done==0) exit = B_TRUE ;
	}
	free(aux_route);
	free(aux_rr.rr);
	return B_FALSE;

}

void set_node_route(node_route * aux_route, long cont,long i, long o_p, long i_p, long channel){
	aux_route[cont].node=i;
	aux_route[cont].o_p=o_p;
	aux_route[cont].channel_out=channel;
	if(i_p != -1){
		aux_route[cont+1].i_p=i_p;
		aux_route[cont+1].channel_in=channel;
	}
}


void set_route(node_route * aux_route, long distance, long pkt_id, long channel_cons){
	long i, node, o_p, i_p, channel_in, channel_out;
	for(i = 0; i <= distance; i++){
		node=aux_route[i].node;
		o_p=aux_route[i].o_p;
		i_p=aux_route[i].i_p;
		channel_in=aux_route[i].channel_in;
		channel_out=aux_route[i].channel_out;
		
		if(o_p == p_con)
			route[node][p_con].lamda[channel_cons]=pkt_id;
		else
			route[node][o_p].lamda[channel_out]=pkt_id;
		if(i > 0)route[node][i_p].lamda[channel_in]=pkt_id;
		if(i == 0)
			route[node][o_p].cont_inj[channel_out]=distance*2; //ida y vuelta camino
		if(plevel & 4) printRoute(node);
	}
	if(plevel & 4)
		printf("T:%lld\tRUTA FIJADA Nodo %ld a Nodo %ld\tpkt_id->%ld\tcont_inj%ld",sim_clock,pkt_space[pkt_id].from, pkt_space[pkt_id].to,pkt_id,(distance*2));
}	
#endif

